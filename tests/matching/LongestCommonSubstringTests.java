package matching;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import components.Converter;
import components.TimedString;
import subtitles.Subtitle;
import video.Video;

public class LongestCommonSubstringTests {

	@Test
	public void LLCSStrTest_NormalCase() {
		String s1 = "abcdefgi";
		String s2 = "avefcdefgj";
		LongestCommonSubstring lcs = new LongestCommonSubstring(s1, s2);
		assertEquals(2, lcs.getPositionInFirstString());
		assertEquals(4, lcs.getPositionInSecondString());
		assertEquals(5, lcs.getLength());
		assertEquals("cdefg", lcs.getSolution());
	}
	
	@Test
	public void LLCSStrTest_reverseCase() {
		String s1 = "abcdefgi";
		String s2 = "avefcdefgj";
		LongestCommonSubstring lcs = new LongestCommonSubstring(s2, s1);
		assertEquals(4, lcs.getPositionInFirstString());
		assertEquals(2, lcs.getPositionInSecondString());
		assertEquals(5, lcs.getLength());
		assertEquals("cdefg", lcs.getSolution());
	}
	
	@Test
	public void LLCSStrTest_ZeroCase() {
		String s1 = "abcdefg";
		String s2 = "hijkl";
		LongestCommonSubstring lcs = new LongestCommonSubstring(s1, s2);
		assertEquals(0, lcs.getPositionInFirstString());
		assertEquals(0, lcs.getPositionInSecondString());
		assertEquals(0, lcs.getLength());
		assertEquals("", lcs.getSolution());
	}
	
	@Test
	public void LLCSStrTest_TrimSpaceCase() {
		String s1 = "ab cdefg  o";
		String s2 = "avef  cdefg  j";
		LongestCommonSubstring lcs = new LongestCommonSubstring(s1, s2);
		assertEquals("cdefg", lcs.getSolution());
		assertEquals(3, lcs.getPositionInFirstString());
		assertEquals(6, lcs.getPositionInSecondString());
		assertEquals(5, lcs.getLength());
		
	}
	
	@Test
	public void LLCSStrTest_TrimWordSplitCase_Beginning() {
		String s1 = "defabc def ghi jkl mno";
		String s2 = "abcabc def ghi jkl pqr";
		LongestCommonSubstring lcs = new LongestCommonSubstring(s1, s2);
		assertEquals("def ghi jkl", lcs.getSolution());
		assertEquals(7, lcs.getPositionInFirstString());
		assertEquals(7, lcs.getPositionInSecondString());
		assertEquals(11, lcs.getLength());
		
	}
	
	@Test
	public void LLCSStrTest_TrimWordSplitCase_End() {
		String s1 = "def def ghi jkl aaamno";
		String s2 = "abc def ghi jkl aaapqr";
		LongestCommonSubstring lcs = new LongestCommonSubstring(s1, s2);
		assertEquals("def ghi jkl", lcs.getSolution());
		assertEquals(4, lcs.getPositionInFirstString());
		assertEquals(4, lcs.getPositionInSecondString());
		assertEquals(11, lcs.getLength());
		
	}
	
	
	@Test
	public void LLCSStrTest_TrimWordSplitCase_Both() {
		String s1 = "defbbb def ghi jkl aaamno";
		String s2 = "abcbbb def ghi jkl aaapqr";
		LongestCommonSubstring lcs = new LongestCommonSubstring(s1, s2);
		assertEquals("def ghi jkl", lcs.getSolution());
		assertEquals(7, lcs.getPositionInFirstString());
		assertEquals(7, lcs.getPositionInSecondString());
		assertEquals(11, lcs.getLength());
		
	}
	
	//D@Test
	public void test(){
		Video video = new Video(new File("tests/testresources/Game of Thrones - s01e01 - Winter Is Coming.mkv"));
		Converter converter = new Converter(video);
		TimedString ts1 = converter.execute();
		TimedString ts2 = new Subtitle(new File("tests/testresources/Game of Thrones - s01e01 - Winter Is Coming.srt")).getSubtitles();
		
		System.out.println(new LongestCommonSubstring(ts1.getImplodedString(), ts2.getImplodedString()));
	}

}
